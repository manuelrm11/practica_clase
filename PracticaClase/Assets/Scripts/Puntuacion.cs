using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Puntuacion : MonoBehaviour
{
    public Text monedas;
    private int cantidadMonedas;

    public Text diamantes;
    private int cantidadDiamantes;

    public Text Salud;
    private float cantidadSalud;
    private const int reduccionVida = 1;

    // Start is called before the first frame update
    void Start()
    {
        cantidadMonedas = 0;
        cantidadDiamantes = 0;
        cantidadSalud = 100;


    }

    // Update is called once per frame
    void Update()
    { 
        monedas.text = cantidadMonedas.ToString();
        diamantes.text = cantidadDiamantes.ToString();

        //Debug.Log(Time.timeSinceLevelLoad);

      
        cantidadSalud -= reduccionVida * Time.deltaTime;
        Debug.Log(cantidadSalud);
        Salud.text = Mathf.RoundToInt(cantidadSalud).ToString();
        
    }
    public void AņadirMonedas()
    {
        cantidadMonedas += 10;
        
    }
    public void AņadirDiamantess()
    {
        cantidadDiamantes += 10;

    }  

    public void ResetearTexto()
    {
        cantidadSalud = 100;
    }
}
